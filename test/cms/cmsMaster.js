const express = require('express');
const cmsMaster = express.Router();
const cmsUserList = require("./cmsUserList");


/**
 * User list
 **/
cmsMaster.post('/user-list', (req, res) => {
    cmsUserList.userList(req, res);
});

cmsMaster.post('/update-user', (req, res) => {
    cmsUserList.updateUser(req, res);
});


module.exports = cmsMaster;
