const CONSTANT = require('../constant/constants');
const error = require('../constant/errorCodes');
const cmsUserListModel = require('../model/cmsUserListModel');


module.exports.userList = function (req, res) {
    if (CONSTANT.DEBUG) console.log(new Date().toISOString() + " userList ++");

    let skip = req.body.skip;
    let limit = req.body.limit;

    if (!(limit && (typeof limit === 'number' || limit instanceof Number) &&
        limit <= CONSTANT.USER_LIST_UPPER_LIMIT && limit >= CONSTANT.USER_LIST_LOWER_LIMIT)) {
        limit = 20;
    }

    if (!(skip && (typeof skip === 'number' || skip instanceof Number) && skip > 0)) {
        skip = 0;
    }

    cmsUserListModel.cmsUserList(skip, limit, (err, result) => {
        if (CONSTANT.DEBUG) console.log("result ++ ");
        if (err) {
            return res.json(err);
        } else if (result && result.length > 0 && result[0].userList.length > 0) {
            if (CONSTANT.DEBUG) console.log("result", JSON.stringify(result));
            result = result[0];
            return res.json({
                data: result
            });
        } else {
            return res.json("No user found");
        }
        if (CONSTANT.DEBUG) console.log("result -- ");
    });
    if (CONSTANT.DEBUG) console.log(new Date().toISOString() + " userList -- ");
};

module.exports.updateUser = function (req, res) {
    if (CONSTANT.DEBUG) console.log(new Date().toISOString() + " updateUser ++");

    let userId = req.body.userId;

    let orderValue;

    if (req.body.updateUser) {
        orderValue = req.body.updateUser.orderValue;
    }

    let cmsUserEditObject = {};
    let projection = {};
    projection["_id"] = 0;

    if (userId) {
        if (!(typeof userId === 'string' || userId instanceof String)) {
            return res.json("User id should be string");
        }
        userId = userId.trim();
        if (!CONSTANT.uuidFilter.test(userId)) {
            return res.json("Invalid user id");
        }
    } else {
        return res.json("Provide user id");
    }

    if (orderValue) {
        if (!(typeof orderValue === 'string' || orderValue instanceof String)) {
            return res.json("Invalid value");
        }
        orderValue = orderValue.trim();
        cmsUserEditObject["noOfOrder"] = orderValue;
    }

    if (Object.keys(cmsUserEditObject).length === 0) {
        return res.json(error.nothingToUpdate);
    }
    if (CONSTANT.DEBUG) console.log(cmsUserEditObject);

    cmsUserListModel.cmsEditUserOrder(userId, cmsUserEditObject, (err, result) => {
        if (err) {
            return res.json(err);
        } else if (result && result.value) {
            return res.json({
                success: true,
                message: "Successfully updated"
            });
        } else {
            return res.json("User id not found");
        }
    });
    if (CONSTANT.DEBUG) console.log(new Date().toISOString() + " updateUser --");
};
