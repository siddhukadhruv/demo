
/*************************************************
 * MongoDB collection names
 ************************************************/

exports.COLLECTION_USERLIST = "userList";
exports.COLLECTION_ORDER = "orderList";

/****************************************************
 * Application config variables
 ***************************************************/
exports.DEBUG = true;
exports.PROD = false;
exports.PORT = 40000;
exports.MONGO_URL = `mongodb://localhost:27017/demo`;

exports.USER_LIST_UPPER_LIMIT = 20;
exports.USER_LIST_LOWER_LIMIT = 0;
exports.uuidFilter = /^[a-zA-Z0-9-]{36}$/;








