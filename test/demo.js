#!/usr/bin/env node
const express = require('express');
const fileUpload = require('express-fileupload');
const demo = express();
const bodyParser = require('body-parser');
const CONSTANT = require('./constant/constants');
const cmsMaster = require('./cms/cmsMaster');
const mongoUtil = require('./utils/mongoUtil');
const readConfigFile = require("./utils/readConfigFile");


demo.use(fileUpload({
    limits: {fileSize: 50 * 1024 * 1024},
    abortOnLimit: true
}));

demo.use(bodyParser.json());
demo.use(bodyParser.urlencoded({extended: true}));

if (process.env.NODE_ENV === 'development' || process.env.NODE_ENV === 'stage' || process.env.NODE_ENV === 'development_s' || process.env.NODE_ENV === 'development_k') {
    demo.use((req, res, next) => {
        res.setHeader('Access-Control-Allow-Origin', '*');
        res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
        res.setHeader('Access-Control-Allow-Headers', 'Content-Type');
        res.setHeader('Access-Control-Allow-Credentials', true);
        next();
    });
}

readConfigFile.readConfigFile((err) => {
    if (err) {
        console.log(" Error reading config file");
        process.exit(1);
    } else {
        mongoUtil.connectToServer((err) => {
            const port = process.env.PORT || CONSTANT.PORT;
            if (err) {
                return console.log(" Error connecting db", err);
            }
                demo.listen(port, (error) => {
                    if (error) {
                        return console.log(new Date() + " Server Down", error);
                    } else {
                        console.log(`${new Date().toISOString()} Connected to port ${port}, env ${process.env.NODE_ENV}`);
                    }
                })
        });
    }
});

demo.use('/cms', cmsMaster);


