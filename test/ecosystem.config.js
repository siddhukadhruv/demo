module.exports = {
  apps: [{
    name: 'demo',
    script: './demo.js',
    exec_interpreter: 'none',
    exec_mode: 'fork_mode',
    env_development: {
      NODE_ENV: 'development'
    },
    env_stage: {
      NODE_ENV: 'stage'
    }
  }]
};
