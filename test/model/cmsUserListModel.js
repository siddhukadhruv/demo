const CONSTANT = require('../constant/constants');
const mongoUtil = require('../utils/mongoUtil');


module.exports.cmsUserList = function (skip, limit, callback) {
    if (CONSTANT.DEBUG) console.log(new Date().toISOString() + " cmsUserList ++");
    mongoUtil.getDatabase().collection(CONSTANT.COLLECTION_USERLIST).aggregate(
        [
            {
                $facet: {
                    totalCount: [
                        {
                            $count: "count"
                        }
                    ],
                    userList: [
                        {
                            $lookup:
                                {
                                    from: CONSTANT.COLLECTION_ORDER,
                                    localField: "userId",
                                    foreignField: "userId",
                                    as: "order"
                                }
                        },
                        {
                            $unwind: {
                                path: "$order",
                                preserveNullAndEmptyArrays: true
                            }
                        },
                        {
                            $project: {
                                _id: 0,
                                userId: 1,
                                name: 1,
                                noOfOrders: 1,
                                subTotal: "$order.subTotal",
                            }
                        },
                        {
                            $skip: skip
                        },
                        {
                            $limit: limit
                        }
                    ]
                }
            },
            {
                $addFields: {
                    totalCount: {$arrayElemAt: ["$totalCount.count", 0]}
                }
            }
        ], callback);
    if (CONSTANT.DEBUG) console.log(new Date().toISOString() + " cmsUserList --");
};

module.exports.cmsEditUserOrder = function (userId, cmsUserEditObject, callback) {
    if (CONSTANT.DEBUG) console.log(" cmsEditUserOrder ++ ", JSON.stringify(cmsEditUserOrder));
    mongoUtil.getDatabase().collection(CONSTANT.COLLECTION_USERLIST).findOneAndUpdate(
        {
            userId: userId
        },
        {
            $set: cmsUserEditObject
        },
        {
            returnOriginal: false,
        }, callback);
    if (CONSTANT.DEBUG) console.log(" cmsEditUserOrder -- ");
};