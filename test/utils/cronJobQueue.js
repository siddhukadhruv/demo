const HashMap = require('hashmap');
const CONSTANT = require('../constant/constants');
let mHashMap = null;

module.exports.initQueue = function (callback) {
    if (CONSTANT.DEBUG) console.log(" hashMapUtils: initQueue ++");
    if (mHashMap === null) {
        mHashMap = new HashMap();
    }
    if (CONSTANT.DEBUG) console.log(" hashMapUtils: initQueue --");
    return callback();
};

module.exports.addJob = function (jobId, job) {
    if (CONSTANT.DEBUG) console.log(" hashMapUtils: addJob ++");
    mHashMap.set(jobId, job);
    if (CONSTANT.DEBUG) console.log(" hashMapUtils: addJob --");
};

module.exports.getJob = function (jobId) {
    if (CONSTANT.DEBUG) console.log(" hashMapUtils: getJob ++");
    if (CONSTANT.DEBUG) console.log(" hashMapUtils: getJob --");
    return mHashMap.get(jobId);
};

module.exports.removeJob = function (jobId) {
    if (CONSTANT.DEBUG) console.log(" hashMapUtils: removeJob ++");
    mHashMap.delete(jobId);
    if (CONSTANT.DEBUG) console.log(" hashMapUtils: removeJob --");
};

module.exports.getHashMap = function () {
    return mHashMap;
};
