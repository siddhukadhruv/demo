const CONSTANT = require('../constant/constants');
const cronJobModel = require("../model/cronJobModel");
const routerSessionJobs = require("../cronJobs/routerSessionJobs");
const validateIPayJob = require("../cronJobs/validateIPayJob");
const bwTrackingJob = require("../cronJobs/bwTrackingJob");
// const dealJobs = require("../cronJobs/dealJobs");

module.exports.restartIncompleteJobsFromDb = function () {
    if (CONSTANT.DEBUG) console.log(" findCronJobs: restartIncompleteJobsFromDb ++");
    cronJobModel.findJobs(CONSTANT.CRON_TYPE_NO_REPEATABLE, CONSTANT.CRON_STATUS_INCOMPLETE, (err, result) => {
        if (err) {
            console.error("err");
            console.error(err);
        } else if (result && result.length > 0) {
            console.log("result");
            console.log(result);
            if (CONSTANT.DEBUG) console.log("Incomplete jobs found, ready for queueing");
            for (let i = 0; i < result.length; i++) {
                if (result[i].category === CONSTANT.CRON_JOB_ROUTER_SESSION_CATEGORY) {
                    routerSessionJobs.startRouterSessionRemoveJob(result[i].jobId, result[i].data, result[i].execTime);
                } else if (result[i].category === CONSTANT.CRON_JOB_VALIDATE_IPAYMENT_CATEGORY) {
                    validateIPayJob.startValidateIPayJob(result[i].jobId, result[i].data, result[i].execTime);
                }
            }
        } else {
            if (CONSTANT.DEBUG) console.log("No jobs found");
        }
    });
    if (CONSTANT.DEBUG) console.log(" findCronJobs: restartIncompleteJobsFromDb --");
};

module.exports.restartRepeatingJobs = function () {
    bwTrackingJob.updateUserBwJob();
};

