const mongoClient = require('mongodb').MongoClient;
const CONSTANT = require('../constant/constants');
let databaseGlobal;

module.exports.connectToServer = (callback) => {
    mongoClient.connect(CONSTANT.MONGO_URL, (error, database) => {
        if (CONSTANT.DEBUG) console.log("Connected correctly to server");

        if (error) {
            return callback(error);
        } else {
            databaseGlobal = database;
            // console.log("databaseGlobal", databaseGlobal);

            userListCollection();
            orderCollection();

            return callback(error);
        }
    });
};

function userListCollection() {
    databaseGlobal.collection(CONSTANT.COLLECTION_USERLIST, {strict: true}, function (err, collection) {
        if (CONSTANT.DEBUG) console.log("COLLECTION_USERLIST ++");
        if (err) {// collection does NOT exists
            if (CONSTANT.DEBUG) console.log("User collection error : " + err);
            //create the collection and then index
            databaseGlobal.createCollection(CONSTANT.COLLECTION_USERLIST, function (err, usersCollection) {
                if (CONSTANT.DEBUG) console.log("Create COLLECTION_USERLIST ++");
                if (err) {
                    if (CONSTANT.DEBUG) console.log('Error creating user collection');
                } else if (usersCollection) {
                    usersCollection.createIndexes([
                        {
                            key: {
                                userId: 1
                            },
                            unique: true,
                            background: true
                        }
                    ]);
                    if (CONSTANT.DEBUG) console.log("Create COLLECTION_USERLIST Indexes --");
                }
                if (CONSTANT.DEBUG) console.log("Create COLLECTION_USERLIST --");
            });
        } else if (collection) { // collection exists
            if (CONSTANT.DEBUG) console.log("Users collection: " + collection.namespace)
        }
        if (CONSTANT.DEBUG) console.log("COLLECTION_USERLIST --");
    });
}

function orderCollection() {
    databaseGlobal.collection(CONSTANT.COLLECTION_ORDER, {strict: true}, function (err, collection) {
        if (CONSTANT.DEBUG) console.log("COLLECTION_ORDER ++");
        if (err) {// collection does NOT exists
            if (CONSTANT.DEBUG) console.log("Order collection error : " + err);
            //create the collection and then index
            databaseGlobal.createCollection(CONSTANT.COLLECTION_ORDER, function (err, orderCollection) {
                if (CONSTANT.DEBUG) console.log("Create COLLECTION_ORDER ++");
                if (err) {
                    if (CONSTANT.DEBUG) console.log('Error creating order collection');
                } else if (orderCollection) {
                    orderCollection.createIndexes([
                        {
                            key: {
                                orderId: 1
                            },
                            unique: true,
                            background: true
                        }
                    ]);
                    if (CONSTANT.DEBUG) console.log("Create COLLECTION_ORDER Indexes --");
                }
                if (CONSTANT.DEBUG) console.log("Create COLLECTION_ORDER --");
            });
        } else if (collection) { // collection exists
            if (CONSTANT.DEBUG) console.log("Order collection: " + collection.namespace)
        }
        if (CONSTANT.DEBUG) console.log("COLLECTION_ORDER --");
    });
}


module.exports.getDatabase = function () {
    return databaseGlobal;
};
