const CONSTANT = require('../constant/constants');
const fs = require('fs');
const path = require('path');

module.exports.readConfigFile = function (callback) {
    if (CONSTANT.DEBUG) console.log(" readConfigFile: readConfigFile ++ " + process.cwd());
    fs.readFile(path.join(process.cwd(), 'runtimeConfig.json'), 'utf-8', async function (error, data) {
        if (error) {
            if (CONSTANT.DEBUG) console.log(" readConfigFile error " + error);
            return callback(error);
        } else if (data) {
            let configFile = JSON.parse(data);
            if (CONSTANT.DEBUG) console.log(" configFile : " + JSON.stringify(configFile));
            await updateConfigValues(configFile[process.env.NODE_ENV]);
            return callback(error);
        }
    });
    if (CONSTANT.DEBUG) console.log(" readConfigFile: readConfigFile --");
};

function updateConfigValues(configFile) {
    if (CONSTANT.DEBUG) console.log(" readConfigFile: updateConfigValues ++");
    CONSTANT.DEBUG = configFile.DEBUG;
    CONSTANT.PROD = configFile.PROD;
    CONSTANT.PORT = configFile.port;
    CONSTANT.MONGO_URL = configFile.mongoUrl;

    if (CONSTANT.DEBUG) console.log(" readConfigFile: updateConfigValues --");
}